﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication7
{
    class Program
    {
        static void Main(string[] args)
        {
            const int iterations = 100000000;
            const string source = "-123456.123";
            FastParser.ParseSingle(source);

            Console.WriteLine("FastParser.ParseSingle [{0}]", FastParser.ParseSingle(source));
            var sw = Stopwatch.StartNew();

            for (int i = 0; i < iterations; i++)
            {
                FastParser.ParseSingle(source);
            }

            Console.WriteLine(sw.Elapsed);
            Console.WriteLine();


            Console.WriteLine("Single.Parse [{0}]", Single.Parse(source));
            sw.Restart();

            for (int i = 0; i < iterations; i++)
            {
                Single.Parse(source);
            }

            Console.WriteLine(sw.Elapsed);
            Console.WriteLine();



            Console.WriteLine("Single.TryParse [{0}]", Single.Parse(source));
            sw.Restart();

            Single outValue;
            for (int i = 0; i < iterations; i++)
            {
                Single.TryParse(source, out outValue);
            }

            Console.WriteLine(sw.Elapsed);
            Console.WriteLine();



            Console.ReadLine();
        }
    }

    class FastParser
    {
        public static Int32 ParseInt32(string source)
        {
            Int32 result = 0;

            bool isNegative = source[0] == '-';

            for (int i = isNegative ? 1 : 0; i < source.Length; i++)
            {
                char ch = source[i];

                if (ch < '0' || ch > '9')
                    throw new InvalidOperationException(String.Format("Character is not numerical value. Character: '{0}' Index: {1}", ch, i));

                result = 10 * result + (source[i] - 48);
            }

            if (isNegative)
                result *= -1;

            return result;
        }

        internal static Single ParseSingle(string source)
        {
            char numberDecimalSeparator = Convert.ToChar(Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator);

            bool isNegative = source[0] == '-';

            bool parsedDecimal = false;
            float result = 0;
            float fraction = 0;
            int fractionStart = -1;

            for (int i = isNegative ? 1 : 0; i < source.Length; i++)
            {
                char ch = source[i];

                if (ch == numberDecimalSeparator)
                {
                    parsedDecimal = true;
                    fractionStart = i;
                }
                else
                {
                    if (ch < '0' || ch > '9')
                        throw new InvalidOperationException(String.Format("Character is not numerical value. Character: '{0}' Index: {1}", ch, i));

                    if (parsedDecimal)
                        fraction = 10 * fraction + (source[i] - 48);
                    else
                        result = 10 * result + (source[i] - 48);
                }
            }

            if (fraction > 0)
            {
                fraction = fraction / _powLookup[source.Length - fractionStart - 1];
                result += fraction;
            }

            if (isNegative) result *= -1;

            return result;
        }

        private static readonly long[] _powLookup = new[]
        {
            1, // 10^0
            10, // 10^1
            100, // 10^2
            1000, // 10^3
            10000, // 10^4
            100000, // 10^5
            1000000, // 10^6
            10000000, // 10^7
            100000000, // 10^8
            1000000000, // 10^9,
            10000000000, // 10^10,
            100000000000, // 10^11,
            1000000000000, // 10^12,
            10000000000000, // 10^13,
            100000000000000, // 10^14,
            1000000000000000, // 10^15,
            10000000000000000, // 10^16,
            100000000000000000, // 10^17,
        };
    }
}
